# **Unity Coding Convention**

This coding convention serves the following purposes:
- Create a consistent look to the code, so that readers can focus on content, not layout.
- Enable readers to understand the code more quickly by making assumptions based on previous experience.
- Facilitate copying, changing, and maintaining the code.
- Demonstrate Coding best practices (SOLID Principles).

Keep in mind basic concepts of OOP
- Inheritance
- Asbtract Classes and Interfaces
- Polymorphism
- Composition over Inheritance
- Good Design Patterns
- **DO NOT use Singletons (Avoid at all costs)**

Law of Demeter:
- Tell Dont Ask
- Try to always Keep it in mind, no need to force it's use 100% of the time
- Avoid Train Wrecks: *obj.GetX().GetY().GetZ().DoSomething();*
- You may call methods of objects that are:
	- Passed as arguments
	- Created locally
	- Instance variables
	- Globals

Try to use SOLID principles of OOP as much as possible  
Keep in mind:
- Single Responsibility
- Open/Close
- Liskov Substitution
- Interface Segregation
- Dependency Inversion


***
## **Naming Conventions**

At all times meaningful names should be used for any and all of the parts of the code, from namespaces to local variables  
Please avoid using Hungarian Notation. If our code architecture is good, a simple naming convention like the one following is perfect.

***
### **Namespaces**
- Namespaces should be Nouns, not verbs
- Namespaces should be named based on the System or Subsystem that is being composed and the libraries that it contains
- Namespaces should be named using **PascalCase**


``` csharp
namespace NotificationSystem { ... }

namespace NotificationSystem.Messages { ... }
```

***
### **Classes and Interfaces**
- Name should be a Noun, not a verb
- Should be named based on the resposibility of the Class/Interface
- Should be named using **PascalCase**
- Manager, there can only be one manager instance of each type
- Controller, there can be multiple instances at the same time
- Abstract Classes should start with the Letter A, followed by the class name
- Interfaces should start with the Letter I, followed by the Interface name


``` csharp
public class GameManager { ... }

public interface IDamageable { ... }

public abstract Class AUnit : IDamageable { ... }

public class PlayerController : AUnit { ... }
```

***
### **Regions**
Regions are a very useful tool to use when the classes get a bit big, however when we follow good coding practices specially when focusing on the Single responsibility princple, classes should not be big, hence not needing to use Regions.
For the exception cases we might use Regions to organize the code a bit better.

- Group Global variables
- Group functionality blocks together
- Group Unity Functions
- Group Class Functions
- Group Debuging Functions and Variables
- Keep the same amount of spaces between Regions (3 lines)
- Regions use **PascalCase**  

> Remember that the use of Regions should not be necessary when following good coding practices, SOLID principles and good software design.


``` csharp
public class SomeClass
{
	#region GlobalVariables

		public int SomeInt;

		public int SomeOtherInt { get; private set; }

		private float _someFloat;

	#endregion GlobalVariables



	#region UnityFunctions

		private void Awake() { }
		private void Start() { }
		private void OnEnable() { }
		...
		private void OnCollisionEnter(...) { }
		..
		private void OnDestroy() { }
	
	#endregion UnityFunction



	#region ClassFunctions

		public void MyPublicFunction() { }

		private void MyPrivateFunction() { }

		...

	#endregion ClassFunctions
}
```

***
### **Variables**
- Public/Internal Variables are named with **PascalCase**
- Private/Protected Variables are named with **_camelCase** (Prepend Underscore)
- Local Variables and Parameters are named with **camelCase**
- Try not to over use Public
	- if a variable needs to be exposed to the inspector and does not need to be public, make it private/protected and add the "SerializeField" decorator
	- Overusing public clutters the API
	- Overusing public might make code-completion slower


``` csharp
public class SomeClass
{
	public int MyIntegerVariable = 0;

	public int MyIntegerProperty { get; private set; }

	[SerializeField]
	private float _mySerializedFloat = 0.5f;

	private float _myFloat = 1f;

	private void MyFunction(string myString)
	{
		bool myBool = false;
	}

	public bool IsActive => myBool;
	
}
```

***
## Layout Convention
- Write only one statement per line.
- Write only one declaration per line.
- If continuation lines are not indented automatically, indent them one tab stop (four spaces).
- Add at least one blank line between method definitions and property definitions.
- Use parentheses to make clauses in an expression apparent, as shown in the following code.

``` csharp
if( (val1 > val2) && (val3 < val4) )
{
	//Code Here
}
```

### Brackets, Spacing, Indentation
- Curly brackets always on a new line
	- Some templating tools / IDEs mess this up so setup the best you can.
- Always include curly brackets (no free-floating ifs)
- Spacing is 4-space-width tab
- Functions with spaces after commas
- Variables always on a new line. 

``` csharp
void MyFunction(int example1, string example2, bool example3)
{
	//Code Here
}
```

### Conditions
- Use [*boolVar == false*] rather than [*!boolVar*] to make intentions **More Explicit**.
- If combining multiple (3 or more) conditions in a single statement consider assigning to a local variable with a descriptive name if it makes sense.
``` csharp
if(boolVal == false)
{
	//Code Here
}
```

***
## Project Organization 

Try to keep Systems that are compiled together inside their own folder structure, instead of having several top folders and having a folder per system inside each of these folders.  
If we treat systems as self contained dependencies we can even create a repository for them and use them as SubModules.  
This allows us to use AsmDef files and will make it very easy to move things to their own Packages, and could potentially greatly decrease compilation times.  

	Asstes
		+ Addons/Tools/Libraries (Self Conatined Projects)
			+ //External Addons if possible
				+ //With their folder structures
				+ //...
			+ Notifications
				+ Scripts
				+ Scenes
				+ Prefabs
				+ ...
			+ Networking
				+ Scripts
				+ Scenes
				+ Prefabs
				+ ...
			+ SomeAdHocSystem
				+ Scripts
				+ Scenes
				+ Prefabs
				+ ...


***
Use interpolating Strings when using .Net 4.x  
**Do Not** concatenate strings
```csharp
private void ProperWay_InterpolatedStrings()
{
	string str1 = "Hello";
	string str2 = "World";

	Debug.Log($"Interpolating these two strings: {str1} {str2}!");
}

private void DoNotConcatStrings()
{
	string str1 = "Hello";
	string str2 = "World";

	Debug.Log("Concatenating these two strings: "+ str1 +" "+ str2);
}
```

***
/// This is under construction...  
/// Let's all complete these guidelines as we go!
